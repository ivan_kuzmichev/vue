import Vue from 'vue'
import contenteditableDirective from 'vue-contenteditable-directive'
import VueColor from 'vue-color'
import App from './App'

Vue.use(contenteditableDirective)
Vue.use(VueColor)
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: { App },
  template: '<App/>'
})
